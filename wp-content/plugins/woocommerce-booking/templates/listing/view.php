<?php
/**
 * Listing View.
 *
 * @package BKAP/view
 */
?>

<!-- Root Element For Listing -->
<div id="<?php echo esc_attr( $id ); ?>" class="bkap_list_booking">
	<div class="tyche_loader">
		<img src="<?php echo esc_url( plugins_url('/woocommerce-booking/assets/images/reschedule-save.gif') ); ?>" alt="Loading">
	</div>
</div>

<!-- Config Booking Listing -->
<script type="text/javascript">
	jQuery( document ).ready(function($) {
		const calendarEl = document.getElementById( '<?php echo $id; ?>' );
		var <?php echo $id; ?> = new FullCalendar.Calendar(calendarEl, {
			noEventsMessage: bkap_data.no_bookable_slot,
			nextDayThreshold : '23:00:00',
			plugins: [ 'rrule', 'dayGrid','list' ],
			header: <?php echo wp_json_encode( $header ); ?>,
			defaultView: '<?php echo $default_view; ?>',
			loading: function (isLoading) {
				bkapListing.loading( isLoading, bkap_data.is_admin );
				if ( 0 !== jQuery( '.tyche_loader').length ) {
					jQuery( '.tyche_loader' ).css( 'display', '' );
					jQuery( '.tyche_loader' ).addClass( 'display-z-index-higher-priority' );
				}
			},
			events: {
				url: '?bkap_events_feed=json&bkap_view=<?php echo $view; ?>',
				method: 'POST',
				extraParams: <?php echo wp_json_encode( $attributes ); ?>,
			},
			eventOrder: 'sort, start,-duration,allDay,title',
			eventRender: function(info) {
				bkapListing.eventRender( info, bkap_data.is_admin );
				if ( 0 !== jQuery( '.tyche_loader').length ) {
					if ( jQuery( '.tyche_loader' ).hasClass( 'display-z-index-higher-priority' ) ) {		
						jQuery( '.tyche_loader' ).removeClass( 'display-z-index-higher-priority' );
					}
					jQuery( '.tyche_loader' ).css("display", "none");
				}
			},
			allDayText: bkap_data.full_day_text,
			datesRender: function( info ) {
				let start = info.view.currentStart;
				if ( moment() >= start  ) {
					$(".fc-prev-button").prop('disabled', true); 
					$(".fc-prev-button").addClass('fc-state-disabled'); 
				} else {
					$(".fc-prev-button").removeClass('fc-state-disabled'); 
					$(".fc-prev-button").prop('disabled', false); 
				}
			},
			firstDay: parseInt( bkap_data.first_day ),
			eventClick: function( info ) { // Adding date param to url when Calendar view
				let cal_view     = [ 'dayGridMonth', 'dayGridWeek', 'dayGridDay' ];
				let view_type    = info.view.type;
				let clicked_date = moment( info.event.start ).format( 'YYYY-MM-DD' );
				if ( info.event.url ) {
					if( cal_view.includes( view_type ) ) {
						window.open( info.event.url + '?bkap_date=' + clicked_date );
					}
				}
			},
			locale: bkap_data.lang,
		});
		<?php echo $id; ?>.render();
	});
</script>
