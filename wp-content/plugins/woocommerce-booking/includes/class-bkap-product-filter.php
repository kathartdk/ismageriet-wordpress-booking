<?php
/**
 * Booking & Appointment Plugin for WooCommerce
 *
 * Class to filter the bookable products on the All products page at the Admin
 *
 * @author      Tyche Softwares
 * @package     BKAP/Bookable-Product-filter
 * @category    Classes
 */


if ( ! class_exists( 'Booking_Filter' ) ) {

	class Booking_Filter {

		/**
		 * Default constructor
		 *
		 * @since 4.11.0
		 */

		public function __construct() {

			add_action( 'restrict_manage_posts', array( $this, 'bkap_custom_product_filters' ) );

			add_action( 'pre_get_posts', array( $this, 'bkap_custom_bookable_filters' ) );

		}

		/**
		 * This function will filter the bookable product with the booking type on the All products page.
		 *
		 * @param string $post_type type of post
		 * @since 4.11.0
		 *
		 * @hook restrict_manage_posts
		 */

		function bkap_custom_product_filters( $post_type ) {

			if ( $post_type == 'product' ) {

				$bookable = $single_day = $multi_days = $date_time = $non_bookable = '';

				if ( isset( $_GET['bookable_filter'] ) ) {

					switch ( $_GET['bookable_filter'] ) {

						case 'bookable':
							$bookable = ' selected';
							break;

						case 'single-day':
							$single_day = ' selected';
							break;

						case 'multi-days':
							$multi_days = ' selected';
							break;

						case 'date-time':
							$date_time = ' selected';
							break;

						case 'non-bookable':
							$non_bookable = ' selected';
							break;
					}
				}

				// Add your filter input here. Make sure the input name matches the $_GET value you are checking above.
				echo '<select name="bookable_filter">';
				echo '<option value>Filter by bookable products</option>';
				echo '<option value="bookable"' . $bookable . '>' . __( 'Bookable Products', 'woocommerce-booking' ) . '</option>';
				echo '<option value="single-day"' . $single_day . '>' . '&rarr;' . __( 'Single Day', 'woocommerce-booking' ) . '</option>';
				echo '<option value="multi-days"' . $multi_days . '>' . '&rarr;' . __( 'Multiple Nights', 'woocommerce-booking' ) . '</option>';
				echo '<option value="date-time"' . $date_time . '>' . '&rarr;' . __( 'Date & Time', 'woocommerce-booking' ) . '</option>';
				echo '<option value="non-bookable"' . $non_bookable . '>' . __( 'Non-bookable products', 'woocommerce-booking' ) . '</option>';
				echo '</select>';
			}
		}


		/**
		 * This function will filter the bookable product with the booking type on the All products page.
		 *
		 * @global string $pagenow current admin page
		 * @param object $query WP_Query Object
		 *
		 * @since 4.11.0
		 *
		 * @hook pre_get_posts
		 */

		function bkap_custom_bookable_filters( $query ) {

			global $pagenow;

			// Ensure it is an edit.php admin page, the filter exists and has a value, and that it's the products page
			if ( $query->is_admin && $pagenow == 'edit.php' && isset( $query->query_vars ) && $query->query_vars['post_type'] == 'product' ) {

				if ( isset( $_GET['bookable_filter'] ) && $_GET['bookable_filter'] == 'bookable' ) {

					$meta_key_query = array(
						array(
							'key'   => '_bkap_enable_booking',
							'value' => 'on',
						),
					);

					$query->query_vars['meta_query'] = $meta_key_query;

				}

				if ( isset( $_GET['bookable_filter'] ) && $_GET['bookable_filter'] == 'single-day' ) {

					$meta_key_query                  = array(
						'relation' => 'AND',
						array(
							'key'   => '_bkap_enable_booking',
							'value' => 'on',
						),
						array(
							'key'   => '_bkap_booking_type',
							'value' => 'only_day',
						),
					);
					$query->query_vars['meta_query'] = $meta_key_query;

				}

				if ( isset( $_GET['bookable_filter'] ) && $_GET['bookable_filter'] == 'multi-days' ) {

					$meta_key_query                  = array(
						array(
							'key'   => '_bkap_booking_type',
							'value' => 'multiple_days',
						),
					);
					$query->query_vars['meta_query'] = $meta_key_query;

				}

				if ( isset( $_GET['bookable_filter'] ) && $_GET['bookable_filter'] == 'date-time' ) {

					$meta_key_query                  = array(
						array(
							'key'   => '_bkap_booking_type',
							'value' => 'date_time',
						),
					);
					$query->query_vars['meta_query'] = $meta_key_query;

				}

				if ( isset( $_GET['bookable_filter'] ) && $_GET['bookable_filter'] == 'non-bookable' ) {

					$meta_key_query                  = array(
						array(
							'key'   => '_bkap_enable_booking',
							'value' => '',
						),
					);
					$query->query_vars['meta_query'] = $meta_key_query;
				}
			}
		}
	}
	$booking_filter = new Booking_Filter();
}


