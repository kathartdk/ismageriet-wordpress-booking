<?php
/**
 * Bookings and Appointment Plugin for WooCommerce
 *
 * Generating ICS file of the booking on Order Receive page and Email notification
 *
 * @author      Tyche Softwares
 * @package     BKAP/ICS
 * @since       2.0
 * @category    Classes
 */

require_once 'bkap-common.php';
require_once 'bkap-lang.php';

if ( ! class_exists( 'bkap_ics' ) ) {

	/**
	 * Class for Generating ICS file of the booking on Order Receive page and Email notification
	 *
	 * @class bkap_ics
	 */

	class bkap_ics {

		/**
		 * Default constructor
		 *
		 * @since 4.1.0
		 */

		public function __construct() {

			$global_settings = json_decode( get_option( 'woocommerce_booking_global_settings' ) );

			// Export date to ics file from order received page
			if ( isset( $global_settings->booking_export ) && $global_settings->booking_export == 'on' ) {
				add_filter( 'woocommerce_order_details_after_order_table', array( 'bkap_ics', 'bkap_export_to_ics' ), 10, 3 );
			}

			// Add order details as an attachment
			if ( isset( $global_settings->booking_attachment ) && $global_settings->booking_attachment == 'on' ) {
				add_filter( 'woocommerce_email_attachments', array( 'bkap_ics', 'bkap_email_attachment' ), 10, 3 );
			}
		}

		  /**
		   * This fuction adds Add to calendar button on Order recieved page which when clicked download the ICS file with booking details.
		   *
		   * @since 1.7
		   * @param object $order Order Object
		   * @global object $wpdb Global wpdb object
		   * @global object $woocommerce Global WooCommerce object
		   */

		public static function bkap_export_to_ics( $order ) {
			global $wpdb;

			if ( isset( $order ) && ( get_class( $order ) == 'WC_Order' || get_class( $order ) == 'Automattic\WooCommerce\Admin\Overrides\Order' ) ) { // woocommerce 3.0.0 checkout page
				$order_id = $order->get_id();
			} elseif ( isset( $order->order_id ) ) { // called from admin end like edit bookings & so on
				$order_id = $order->order_id;
			} elseif ( isset( $order ) && ( get_class( $order ) == 'WC_Order' || get_class( $order ) == 'Automattic\WooCommerce\Admin\Overrides\Order' ) && isset( $order->id ) ) { // woocommerce version < 3.0.0
				$order_id = $order->id;
			}

			if ( isset( $order_id ) && 0 != $order_id ) {
				$order_obj   = new WC_Order( $order_id );
				$order_items = $order_obj->get_items();

				$today_query  = 'SELECT * FROM `' . $wpdb->prefix . 'booking_history` AS a1,`' . $wpdb->prefix . 'booking_order_history` AS a2
                                    WHERE a1.id = a2.booking_id AND a2.order_id = %d';
				$results_date = $wpdb->get_results( $wpdb->prepare( $today_query, $order_id ) );

				if ( $results_date ) {

					foreach ( $order_items as $item_key => $item_value ) {

						$duplicate_of     = bkap_common::bkap_get_product_id( $item_value['product_id'] );
						$booking_settings = bkap_common::bkap_product_setting( $item_value['product_id'] );

						if ( isset( $item_value['wapbk_booking_date'] ) && $item_value['wapbk_booking_date'] != '' ) {

							if ( isset( $booking_settings['booking_enable_date'] )
								&& $booking_settings['booking_enable_date'] == 'on'
								&& isset( $booking_settings['booking_enable_multiple_day'] )
								&& $booking_settings['booking_enable_multiple_day'] == ''
							) {

								for ( $c = 0; $c < count( $results_date ); $c++ ) {

									$bkap_booking_date = $item_value['wapbk_booking_date'];

									$duration_time = false;
									if ( strpos( $bkap_booking_date, ' - ' ) !== false ) {
										$duration_time     = true;
										$duration_dates    = explode( ' - ', $item_value['wapbk_booking_date'] );
										$bkap_booking_date = $duration_dates[0];
									}

									if ( $results_date[ $c ]->post_id == $duplicate_of
										&& $bkap_booking_date == $results_date[ $c ]->start_date
									) {

										$booked_product = array();
										$dt             = strtotime( $results_date[ $c ]->start_date );
										$time           = 0;
										$time_start     = 0;
										$time_end       = 0;

										if ( isset( $booking_settings['booking_enable_time'] )
											&& ( $booking_settings['booking_enable_time'] == 'on' || $booking_settings['booking_enable_time'] == 'duration_time' )
										) {

											if ( isset( $item_value['wapbk_time_slot'] ) && '' != $item_value['wapbk_time_slot'] ) {
												$order_start_time = $order_end_time = '';
												$order_time_slot  = explode( ' - ', $item_value['wapbk_time_slot'] );

												$order_start_time = $order_time_slot[0];
												$order_end_time   = isset( $order_time_slot[1] ) ? $order_time_slot[1] : '';

												if ( $results_date[ $c ]->from_time != $order_start_time
													&& $results_date[ $c ]->to_time != $order_end_time
												) {
													continue;
												}
											}

											$time_start = explode( ':', $results_date[ $c ]->from_time );
											if ( $results_date[ $c ]->to_time != '' ) {
												$time_end = explode( ':', $results_date[ $c ]->to_time );
											}

											if ( isset( $time_start[0] ) && isset( $time_start[1] ) ) {
												$time_start = $time_start[0] * 60 * 60 + $time_start[1] * 60;
											}

											if ( isset( $time_end[0] ) && isset( $time_end[1] ) ) {
												$time_end = $time_end[0] * 60 * 60 + $time_end[1] * 60;
											}
										}

										$start_timestamp = $dt + $time_start + ( time() - current_time( 'timestamp' ) );

										if ( $duration_time ) {
											$end_timestamp = strtotime( $duration_dates[1] ) + $time_end + ( time() - current_time( 'timestamp' ) );
											if ( $time_end > 0 ) {
												$end_timestamp = strtotime( $duration_dates[1] ) + $time_end + ( time() - current_time( 'timestamp' ) );
											}
										} else {
											$end_timestamp = $start_timestamp;
											if ( $time_end > 0 ) {
												$end_timestamp = $dt + $time_end + ( time() - current_time( 'timestamp' ) );
											}
										}

										$booked_product['start_timestamp'] = $start_timestamp;
										$booked_product['end_timestamp']   = $end_timestamp;
										$booked_product['name']            = $item_value['name'];

										self::bkap_ics_booking_details_form( $booked_product );
									}
								}
							} elseif ( isset( $booking_settings['booking_enable_date'] ) && $booking_settings['booking_enable_date'] == 'on' && isset( $booking_settings['booking_enable_multiple_day'] ) && $booking_settings['booking_enable_multiple_day'] == 'on' ) {

								for ( $c = 0; $c < count( $results_date ); $c++ ) {

									if ( $results_date[ $c ]->post_id == $duplicate_of ) {

										$booked_product                    = array();
										$booked_product['start_timestamp'] = strtotime( $results_date[ $c ]->start_date );
										$booked_product['end_timestamp']   = strtotime( $results_date[ $c ]->end_date );
										$booked_product['name']            = $item_value['name'];
										self::bkap_ics_booking_details_form( $booked_product );
									}
								}
							}
						}
					}
				}
			}

		}

		/**
		 * This function attach the ICS file with the booking details in the email sent to user and admin.
		 *
		 * @since 1.7
		 * @param array   $other Empty array.
		 * @param object  $email_id ID of email template.
		 * @param object  $order Order Object.
		 * @global object $wpdb Global wpdb object.
		 * @global object $woocommerce Global WooCommerce object.
		 *
		 * @return $file Returns the ICS file for the booking.
		 */
		public static function bkap_email_attachment( $other, $email_id, $order ) {
			global $wpdb;

			if ( isset( $order ) && ( get_class( $order ) == 'WC_Order' || get_class( $order ) == 'Automattic\WooCommerce\Admin\Overrides\Order' ) ) { // woocommerce 3.0.0 checkout page
				$order_id = $order->get_id();
			} elseif ( isset( $order->order_id ) ) { // called from admin end like edit bookings & so on.
				$order_id = $order->order_id;
			} elseif ( isset( $order ) && ( get_class( $order ) == 'WC_Order' || get_class( $order ) == 'Automattic\WooCommerce\Admin\Overrides\Order' ) && isset( $order->id ) ) { // woocommerce version < 3.0.0
				$order_id = $order->get_id();
			}

			if ( isset( $order_id ) && 0 != $order_id ) {

				$order_obj   = new WC_Order( $order_id );
				$order_items = $order_obj->get_items();

				$today_query  = 'SELECT * FROM `' . $wpdb->prefix . 'booking_history` AS a1,`' . $wpdb->prefix . 'booking_order_history` AS a2 WHERE a1.id = a2.booking_id AND a2.order_id = %d';
				$results_date = $wpdb->get_results( $wpdb->prepare( $today_query, $order_id ) );

				$file_path = WP_CONTENT_DIR . '/uploads/wbkap_tmp';
				$file      = array();
				$c         = 0;

				foreach ( $order_items as $item_key => $item_value ) {

					$duplicate_of = get_post_meta( $item_value['product_id'], '_icl_lang_duplicate_of', true );

					if ( $duplicate_of == '' && $duplicate_of == null ) {
						$post_time       = get_post( $item_value['product_id'] );
						$id_query        = 'SELECT ID FROM `' . $wpdb->prefix . 'posts` WHERE post_date = %s ORDER BY ID LIMIT 1';
						$results_post_id = $wpdb->get_results( $wpdb->prepare( $id_query, $post_time->post_date ) );

						if ( isset( $results_post_id ) ) {
							$duplicate_of = $results_post_id[0]->ID;
						} else {
							$duplicate_of = $item_value['product_id'];
						}
					}

					$booking_settings = get_post_meta( $item_value['product_id'], 'woocommerce_booking_settings', true );
					$file_name        = get_option( 'book_ics-file-name' );

					if ( isset( $item_value['wapbk_booking_date'] ) && $item_value['wapbk_booking_date'] != '' ) {

						$bkap_calendar_sync = new bkap_calendar_sync();
						$app                = $bkap_calendar_sync->bkap_create_gcal_obj( $item_value->get_id(), $item_value, $order_obj );

						if ( ( isset( $booking_settings['booking_enable_date'] )
							&& $booking_settings['booking_enable_date'] == 'on' )
							&& ( isset( $booking_settings['booking_enable_multiple_day'] ) && $booking_settings['booking_enable_multiple_day'] == '' )
						) {
							for ( $c = 0; $c < count( $results_date ); $c++ ) {

								if ( $results_date[ $c ]->post_id == $duplicate_of ) {

									$booked_product    = array();
									$bkap_booking_date = $item_value['wapbk_booking_date'];
									$duration_time     = false;
									if ( strpos( $bkap_booking_date, ' - ' ) !== false ) {
										$duration_time  = true;
										$duration_dates = explode( ' - ', $item_value['wapbk_booking_date'] );
									}

									$dt   = strtotime( $results_date[ $c ]->start_date );
									$time = $time_start_value = $time_end_value = 0;

									if ( $booking_settings['booking_enable_time'] == 'on' || $booking_settings['booking_enable_time'] == 'duration_time' ) {
										$time_start = explode( ':', $results_date[ $c ]->from_time );
										$time_end   = explode( ':', $results_date[ $c ]->to_time );

										if ( isset( $time_start[0] ) && isset( $time_start[1] ) ) {
											$time_start_value = $time_start[0] * 60 * 60 + $time_start[1] * 60;
										}

										if ( isset( $time_end[0] ) && isset( $time_end[1] ) ) {
											$time_end_value = $time_end[0] * 60 * 60 + $time_end[1] * 60;
										}
									}

									$start_timestamp = $dt + $time_start_value + ( time() - current_time( 'timestamp' ) );

									if ( $duration_time ) {
										$end_timestamp = strtotime( $duration_dates[1] ) + $time_end_value + ( time() - current_time( 'timestamp' ) );
										if ( $time_end > 0 ) {
											$end_timestamp = strtotime( $duration_dates[1] ) + $time_end_value + ( time() - current_time( 'timestamp' ) );
										}
									} else {
										$end_timestamp = $start_timestamp;
										if ( $time_end_value > 0 ) {
											$end_timestamp = $dt + $time_end_value + ( time() - current_time( 'timestamp' ) );
										}
									}

									$booked_product['start_timestamp'] = $start_timestamp;
									$booked_product['end_timestamp']   = $end_timestamp;
									$booked_product['name']            = $item_value['name'];

									$description = str_replace(
										array( 'SITE_NAME', 'CLIENT', 'PRODUCT_NAME', 'PRODUCT_WITH_QTY', 'ORDER_DATE_TIME', 'ORDER_DATE', 'ORDER_NUMBER', 'PRICE', 'PHONE', 'NOTE', 'ADDRESS', 'EMAIL', 'RESOURCE', 'ZOOM_MEETING' ),
										array( get_bloginfo( 'name' ), $app->client_name, $app->product, $app->product_with_qty, $app->order_date_time, $app->order_date, $app->id, $app->order_total, $app->client_phone, $app->order_note, $app->client_address, $app->client_email, $app->resource, $app->zoom_meeting ),
										get_option( 'bkap_calendar_event_description' )
									);
									$booked_product['summary']         = $description;
									if ( ! file_exists( $file_path ) ) {
										mkdir( $file_path, 0777 );
									}
									$file[ $c ] = $file_path . '/' . $file_name . '_' . $c . '.ics';
									$current    = self::bkap_ics_booking_details_email( $booked_product ); // Append a new person to the file
									file_put_contents( $file[ $c ], $current ); // Write the contents back to the file
								}
							}
						} elseif ( ( isset( $booking_settings['booking_enable_date'] ) && $booking_settings['booking_enable_date'] == 'on' )
							&& ( isset( $booking_settings['booking_enable_multiple_day'] ) && $booking_settings['booking_enable_multiple_day'] == 'on' )
						) {

							for ( $c = 0; $c < count( $results_date ); $c++ ) {

								if ( $results_date[ $c ]->post_id == $duplicate_of ) {

									$booked_product                    = array();
									$booked_product['start_timestamp'] = strtotime( $results_date[ $c ]->start_date );
									$booked_product['end_timestamp']   = strtotime( $results_date[ $c ]->start_date );
									$booked_product['name']            = $item_value['name'];
									$description = str_replace(
										array( 'SITE_NAME', 'CLIENT', 'PRODUCT_NAME', 'PRODUCT_WITH_QTY', 'ORDER_DATE_TIME', 'ORDER_DATE', 'ORDER_NUMBER', 'PRICE', 'PHONE', 'NOTE', 'ADDRESS', 'EMAIL', 'RESOURCE', 'ZOOM_MEETING' ),
										array( get_bloginfo( 'name' ), $app->client_name, $app->product, $app->product_with_qty, $app->order_date_time, $app->order_date, $app->id, $app->order_total, $app->client_phone, $app->order_note, $app->client_address, $app->client_email, $app->resource, $app->zoom_meeting ),
										get_option( 'bkap_calendar_event_description' )
									);
									$booked_product['summary']         = $description;

									self::bkap_ics_booking_details_email( $booked_product );

									if ( ! file_exists( $file_path ) ) {
										mkdir( $file_path, 0777 );
									}
									$file[ $c ] = $file_path . '/' . $file_name . '_' . $c . '.ics';
									$current    = self::bkap_ics_booking_details_email( $booked_product ); // Append a new person to the file
									file_put_contents( $file[ $c ], $current ); // Write the contents back to the file
								}
							}
						}
					}
				}
				return $file;
			}
		}

		/**
		 * This function add hidden fields for booking details and Add to Calendar button on the Thank you page.
		 *
		 * @since 1.7
		 * @param array $booked_product Array of booking details and product name.
		 */

		public static function bkap_ics_booking_details_form( $booked_product ) {
			?>
			<form method="post" action="<?php echo plugins_url( '/bkap-export-ics.php', __FILE__ ); ?>" id="export_to_ics">    
				<input type="hidden" id="book_date_start"  name="book_date_start"  value="<?php echo $booked_product['start_timestamp']; ?>" />
				<input type="hidden" id="book_date_end"    name="book_date_end"    value="<?php echo $booked_product['end_timestamp']; ?>" />
				<input type="hidden" id="current_time"     name="current_time"     value="<?php echo current_time( 'timestamp' ); ?>" />
				<input type="hidden" id="book_name"        name="book_name"        value="<?php echo $booked_product['name']; ?>" />    
				<input type="submit" id="exp_ics"          name="exp_csv"          value="<?php _e( 'Add to Calendar', 'woocommerce-booking' ); ?>" /> (<?php echo $booked_product['name']; ?>)
			</form>
			<?php
		}

		/**
		 * This function create the string required to create the ICS file with the booking details.
		 *
		 * @since 1.7
		 * @param array $booked_product Array of booking details and product name.
		 *
		 * @return string $icsString Returns the string of the ICS file for the booking
		 */
		public static function bkap_ics_booking_details_email( $booked_product ) {

			$description = $booked_product['summary'];
			$description = str_replace( '\x0D', '', $description ); // lf - html break.
			$description = preg_replace( "/\r|\n/", "", $description );
			
			$ics_string = 'BEGIN:VCALENDAR
PRODID:-//Events Calendar//iCal4j 1.0//EN
VERSION:2.0
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTSTART:' . date( 'Ymd\THis\Z', $booked_product['start_timestamp'] ) . '
DTEND:' . date( 'Ymd\THis\Z', $booked_product['end_timestamp'] ) . '
DTSTAMP:' . date( 'Ymd\THis\Z', current_time( 'timestamp' ) ) . '
UID:' . ( uniqid() ) . '
DESCRIPTION:' . $description . '
SUMMARY:' . $booked_product['name'] . '
END:VEVENT
END:VCALENDAR';

			return $ics_string;
		}

	} // End of class
	$bkap_ics = new bkap_ics();
}
?>
