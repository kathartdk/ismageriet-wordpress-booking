/**
 * This function allows to dismiss the notices which are shown from the plugin.
 *
 * @namespace bkap_notice_dismissible
 * @since 6.8
 */
// Make notices dismissible
jQuery(document).ready( function() {
	/**
	 * Dismiss BKAP TS tracking notice
	 */
	jQuery('.bkap-tracker').on( 'click', 'button.notice-dismiss', function() {
		var data = { action: "bkap_admin_notices" };
		jQuery.post( ts_dismiss_notice.ts_admin_url, data, function( response ) {
		});
	});


	/**
	 * Dismiss Booking timeslot list view feature notice.
	 */
	jQuery('.bkap-timeslot-notice').on( 'click', 'button.notice-dismiss', function() {
		var data = { notice: "bkap-timeslot-notice", action: "bkap_dismiss_admin_notices" };
		jQuery.post( ajaxurl, data, function( response ) {
		});
	});

	/**
	 * Dismiss Zoom Meeting notice
	 */
	jQuery('.bkap-meeting-notice').on( 'click', 'button.notice-dismiss', function() {		
		var data = { notice: "bkap-meeting-notice", action: "bkap_dismiss_admin_notices" };		
		jQuery.post( ajaxurl, data, function( response ) {
		});
	});
});