<?php

/**
 * Ismageriet-child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Ismageriet-child
 * @since 0.0.5
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ISMAGERIET_CHILD_VERSION', '0.0.5' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
	wp_enqueue_style( 'ismageriet-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ISMAGERIET_CHILD_VERSION, 'all' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script( 'child-theme', get_stylesheet_directory_uri() . '/inc/child-theme.js', array( 'jquery' ),'',true );

});


include_once( 'inc/acf-settings.php' );
include_once( 'inc/woo-settings.php' );
include_once( 'inc/woo-cart-rules.php' );
include_once( 'inc/woo-composite-product-settings.php' );
include_once( 'inc/woo-restapi.php' );

// disable ‘UAG Templates’ button from all post, pages and other post types.
add_filter( 'ast_block_templates_disable', '__return_true' );

//Gutenberg settings
include_once( 'inc/gutenberg-settings.php' );

// Custom login styles
function my_custom_login()
{
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login-styles.css" />';
}
add_action('login_head', 'my_custom_login');


// Add backend styles for Gutenberg.
add_action( 'enqueue_block_editor_assets', 'photographus_add_gutenberg_assets' );
//Load Gutenberg stylesheet.
function photographus_add_gutenberg_assets() {
	// Load the theme styles within Gutenberg.
	wp_enqueue_style( 'photographus-gutenberg', get_theme_file_uri( 'gutenberg-styles.css' ), false );
}


//---------------------------------------------------------------------
// Add user id CSS class via http://codex.wordpress.org/Function_Reference/body_class
add_filter( 'body_class', 'add_user_class_id' );
function add_user_class_id( $classes ) {

	global $current_user;
	$user_ID = $current_user->ID;
	$classes[] = 'user-id-' . $user_ID;
	// return the $classes array
	return $classes;
}






// checking for composite product data
add_filter( 'woocommerce_checkout_cart_item_visible', 'wc_cp_cart_item_visible' , 10, 3 );
if( !function_exists('wc_cp_cart_item_visible')){
    function wc_cp_cart_item_visible($visible, $cart_item, $cart_item_key){
        if( WC()->session->get('ugens-isvariant-besked' ) ){
            add_filter('wc_od_delivery_date_args', 'cb_datepicker_edit_db',99 ,1);
        }
        return $visible;
    }
}


//--------------------------------------
// Add product cat name to body class
add_filter( 'body_class', 'kathart_wc_product_cats_css_body_class' );

function kathart_wc_product_cats_css_body_class( $classes ){
  if ( is_singular( 'product' ) ) {
    $current_product = wc_get_product();
    $custom_terms = get_the_terms( $current_product->get_id(), 'product_cat' );
    if ( $custom_terms ) {
      foreach ( $custom_terms as $custom_term ) {
        $classes[] = 'product_cat_' . $custom_term->slug;
      }
    }
  }
  return $classes;
}



// --------------------------------------------------------------------
// check for a certain meta key on the current post and add a body class if meta value exists
// TODO - Check if this is in use anymore
add_filter('body_class','krogs_custom_field_body_class');
function krogs_custom_field_body_class( $classes ) {

	if ( get_post_meta( get_the_ID(), 'page_transparant_background', true ) ) {
		
		$classes[] = 'transparant_container';
		
	}
	
	// return the $classes array
	return $classes;
}




// -------------------------------------
// Show Phone number in the checkout page
add_filter( 'woocommerce_checkout_fields' , 'custom_phone_checkout_field' );

// Our hooked in function – $fields is passed via the filter!
function custom_phone_checkout_field( $fields ) {
     $fields['billing']['billing_phone'] = array(
        'label'     => __('Telefon', 'woocommerce'),
        'placeholder'   => _x('Telefon', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide'),
        'clear'     => true,
        'priority'	=> 90,
     );
     
     $fields['shipping']['shipping_phone'] = array(
        'label'     => __('Telefon', 'woocommerce'),
        'placeholder'   => _x('Telefon', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true,
        'priority'	=> 90,
     );
     

     return $fields;
}


// Display phone field value on the order edit page
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'checkout_field_display_admin_order_meta', 10, 1 );

function checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Telefon fra checkout formularen').':</strong> ' . get_post_meta( $order->get_id(), '_billing_phone', true ) . '</p>';
}


//-----------------------------------------
// Unhook all Woo Emails template
// TODO - Turn this one before the Webshop goes live 

add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );

function unhook_those_pesky_emails( $email_class ) {

		/**
		 * Hooks for sending emails during store events
		 **/
		remove_action( 'woocommerce_low_stock_notification', array( $email_class, 'low_stock' ) );
		remove_action( 'woocommerce_no_stock_notification', array( $email_class, 'no_stock' ) );
		remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );
		
		// New order emails
		remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		
		// Processing order emails
		remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
		
		// Completed order emails
		remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );
			
		// Note emails
		remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
.woocommerce-page.ac-shop_order.post-type-shop_order #stockModal {display:none !important;}
.woocommerce-page.ac-shop_order.post-type-shop_order #priceModal {display:none !important;}



.woocommerce-page.post-type-shop_coupon #stockModal {display:none !important;}
.woocommerce-page.post-type-shop_coupon #priceModal {display:none !important;}


  </style>';
}

