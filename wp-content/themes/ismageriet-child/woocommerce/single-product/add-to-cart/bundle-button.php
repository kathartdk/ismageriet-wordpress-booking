<?php
/**
 * Bundle add-to-cart button template
 *
 * Override this template by copying it to 'yourtheme/woocommerce/single-product/add-to-cart/bundle-button.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 6.11.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

?>

<?php if ( is_single( '202011' ) ): ?>
	<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button bundle_add_to_cart_button button alt">Læg i kurv</button>
	<div class="button-wrapper"> <a class="button cta-button bundle-no-thanks" href= "/produkt/tilbehoer/">Jeg vil ikke have drys</a> </div>
<?php elseif ( is_single( '203039' ) ): ?>
	<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button bundle_add_to_cart_button button alt">Læg i kurv</button>
	<div class="button-wrapper"> <a class="button cta-button bundle-no-thanks" href= "/produkt/tilbehoer-udenske/">Jeg vil ikke have drys</a> </div>
<?php else: ?>
	<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button bundle_add_to_cart_button button alt">Læg i kurv</button>
	<div class="button-wrapper"> <a class="button cta-button bundle-no-thanks" href= "/kurv">Jeg vil ikke have tilbehør</a> </div>
<?php endif;?>
