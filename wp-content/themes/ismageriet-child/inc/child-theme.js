jQuery('document').ready(function(){
	jQuery('.product_cat_literbokse .composite_summary .step_title_wrapper').find('.step_title').text('Din literboks');

	jQuery('.product_cat_bygselvkage .composite_summary .step_title_wrapper').find('.step_title').text('Din islagkage');

	jQuery('.product_cat_favoritter-islagkage .composite_summary .step_title_wrapper').find('.step_title').text('Din islagkage');

	adjust_composite_product_images();
});

jQuery('body').on('updated_cart_totals', function(){
    adjust_composite_product_images();
});

function adjust_composite_product_images(){
	jQuery(".woocommerce-cart-form__cart-item.cart_item.component_container_table_item").each(function(index,element){
		jQuery(element).addClass('parent-'+index);
		jQuery(element).nextUntil('tr.component_container_table_item').addClass('cpy copy-to-'+index);
		jQuery('.cpy:not(.component_table_item)').removeAttr('class');
		jQuery(".cart_item.component_table_item.copy-to-"+index).appendTo(".parent-"+index+" .product-name");
	});
}

//-----------------
// TODO - DONT IN USE. DELETE MAYBE
jQuery('document').ready(function(){
	jQuery("li input[type='radio']").on( 'click', function(){

		if (jQuery("#shipping_method_0_local_pickup2").is(":checked")) {
			jQuery("#wc-od h3").html( "Afhentnings info søborg" );
		} else if (jQuery("#shipping_method_0_free_shipping3").is(":checked")) {
			jQuery("#wc-od h3").html( "Leverings info" );
		}
	});
});


jQuery(document).ready(function(){

	jQuery(document).on('click', '.component_option_radio_button .tool', function(e){

		e.stopPropagation();

		var $item = jQuery(this).next().addClass('current')

		jQuery('.thumbnal-short-description.active').not('.current').removeClass('active');

		$item.toggleClass('active').removeClass('current');

	})


	jQuery(document).on('click', function(e){
		if(!jQuery(e.target).hasClass('tool') && !jQuery(e.target).closest('.thumbnal-short-description').length){
			jQuery('.thumbnal-short-description.active').removeClass('active');
		}
	});
})



jQuery(document).ready(function(){

	jQuery(document).on('click', '.component_option_radio_button_container',  function(){
	    var color = jQuery(this).find('.product-color').attr('style');
	    var $block = jQuery('.summary_element_wrapper.selected');

	    if(!$block.children('.color-background-helper').length){
	    	$block.append(jQuery("<div>",{class:'color-background-helper'}).on('click', function(){
	    		jQuery(this).closest('.summary_element_wrapper').find('a img').trigger('click');
	    	}));
	    }

		jQuery('.woocommerce-product-gallery').hide();
		jQuery('.woocommerce-product-details__short-description').hide();
		jQuery('.summary.entry-summary').addClass( 'full-bredde' );

	    jQuery('.summary_element_wrapper.selected').children('.color-background-helper').attr('style',color);


	    //composite_control_object
		var find = false;
		jQuery('.summary_element_wrapper.selected').closest('li').nextAll().each(function(){
			if(!find){
				if(!jQuery(this).find('.summary_element_data').length){
					var helper = jQuery(this).index();

					setTimeout(function(){
							composite_control_object.navigate_to_step(composite_control_object.get_steps()[helper]);
					},300);

					find = true;
				}
			}
		});

		if(!find){
			setTimeout(function(){
				composite_control_object.navigate_to_step(composite_control_object.get_steps()[composite_control_object.get_steps().length-1]);
			},300);
		}

		if(jQuery('.summary_element_wrapper.disabled').length == 0){
			jQuery('.wc-pao-addons-container, .composite_wrap .composite_add_to_cart_button').addClass('activetilbehor');
		}


	});




	jQuery('button[name=shipping_type]').on('click', function(e){
	    e.preventDefault();
	    var value = jQuery(this).val().trim().toLowerCase();

	    jQuery('#wcmlim-change-lc-select option').each(function(){
	    	if(jQuery(this).text().trim().toLowerCase() == value){
	    	    jQuery(this).prop('selected',true).closest('select');
	    	}
		});
		
		//page-id is the Bestil is page
		var data = jQuery('.page-id-51 #lc-switch-form').serialize();
		jQuery.ajax({
		    data:data,
		    method:"POST",
		    url:location.href,
		    success:function(){
		        location.href = '/bestil-is/';
		    }
		})

	});

	jQuery('.click-soborg').on('click', function(e){
	    e.preventDefault();
	    var value = jQuery(this).val().trim().toLowerCase();

	    jQuery('#wcmlim-change-lc-select option').each(function(){
	    	if(jQuery(this).text().trim().toLowerCase() == value){
	    	    jQuery(this).prop('selected',true).closest('select');
	    	}
		});
		
		//page-id is the Afhentnings-sted page
		var data = jQuery('.page-id-192357 #lc-switch-form').serialize();
		jQuery.ajax({
		    data:data,
		    method:"POST",
		    url:location.href,
		    success:function(){
		        location.href = '/afhentnings-sted/afhent-i-soeborg/';
		    }
		})

	});

	jQuery('.click-amager').on('click', function(e){
	    e.preventDefault();
	    var value = jQuery(this).val().trim().toLowerCase();

	    jQuery('#wcmlim-change-lc-select option').each(function(){
	    	if(jQuery(this).text().trim().toLowerCase() == value){
	    	    jQuery(this).prop('selected',true).closest('select');
	    	}
		});

		//page-id is the Afhentnings-sted page
		var data = jQuery('.page-id-192357 #lc-switch-form').serialize();
		jQuery.ajax({
		    data:data,
		    method:"POST",
		    url:location.href,
		    success:function(){
		        location.href = '/afhentnings-sted/afhent-paa-amager/';
		    }
		})

	});

});



var composite_control_object;

(function($, window, document, undefined ){
	$( '.composite_data' ).on( 'wc-composite-initializing', function(event, composite){
		composite_control_object = composite;
	});
})(jQuery, window, document);


// Plus and minus signs on the addonds
jQuery(function(){
	jQuery(".btn-addon").on("click", function() {
	  var $button = jQuery(this);
	  var oldValue = $button.parent().find("input").val();
  
	  if ($button.text() == "+") {
		var newVal = parseFloat(oldValue) + 1;
	  } else {
		if (oldValue > 0) {
		  var newVal = parseFloat(oldValue) - 1;
		} else {
		  newVal = 0;
		}
	  }
  
	  $button.parent().find("input").val(newVal);
	});
	
  });
  