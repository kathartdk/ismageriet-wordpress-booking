<?php

//Unregister block patterns settings
remove_theme_support('core-block-patterns');


//Custom gutenberg
//include_once('gutenberg/buttons.php');
include_once('gutenberg/table.php');
include_once('gutenberg/heading.php');
//include_once('gutenberg/list.php');
//include_once('gutenberg/paragraph.php');
include_once('gutenberg/patterns.php');



/**
 * Theme Setup
 *
 */
function ea_setup() {
	// Disable Custom Colors
	//add_theme_support( 'disable-custom-colors' );

	// Editor Color Palette
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'blue', 'ea-starter' ),
			'slug'  => 'blue',
			'color'	=> '#59BACC',
		),
		array(
			'name'  => __( 'Green', 'ea-starter' ),
			'slug'  => 'green',
			'color' => '#21A07A',
		),
		array(
			'name'  => __( 'Lightgreen', 'ea-starter' ),
			'slug'  => 'lightgreen',
			'color' => '#e2f7f1',
		),
		array(
			'name'	=> __( 'Brown', 'ea-starter' ),
			'slug'	=> 'brown',
			'color'	=> '#58320e',
		),
        array(
			'name'	=> __( 'Yellow', 'ea-starter' ),
			'slug'	=> 'yellow',
			'color'	=> '#FFBA00',
		),
        array(
			'name'	=> __( 'Red', 'ea-starter' ),
			'slug'	=> 'red',
			'color'	=> '#C80033',
		),
        array(
			'name'	=> __( 'Beige', 'ea-starter' ),
			'slug'	=> 'beige',
			'color'	=> '#fdf5dc',
		),
        array(
			'name'	=> __( 'Black', 'ea-starter' ),
			'slug'	=> 'black',
			'color'	=> '#000000',
		),
	) );
}
add_action( 'after_setup_theme', 'ea_setup' );

// Opret ny kategori - Kathart Blocks
add_filter('block_categories', 'kathart_block_category', 10, 2);
function kathart_block_category($categories, $post)
{
    return array_merge(
        $categories,
        [
            [
                'slug' => 'kathart-blocks',
                'title' => __('Kathart Blocks', 'kathart-blocks'),
            ],
        ]
    );
}


function my_acf_admin_head() {
    ?>
    <style type="text/css">

        [data-type*="bestil-forside-block"] .wp-block[data-align=right]>* {
            float: right;
            margin-left: 0em /*!rtl:end:ignore*/;
        }
        [data-type*="bestil-forside-block"] .wp-block[data-align=left]>* {
            float: left;
            margin-right: 0em /*!rtl:end:ignore*/;
        }
    </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');
