<?php

//----------------------------------------
// Some Composite Product filters snippets / https://docs.woocommerce.com/document/composite-products/composite-products-snippets/ 
add_filter( 'woocommerce_composite_sequential_comp_progress', 'wc_cp_sequential_componentized_progress', 10, 2 );
    function wc_cp_sequential_componentized_progress( $seq, $composite ) {
	return 'yes';
}

add_filter( 'woocommerce_component_options_per_page', 'wc_cp_component_options_per_page', 10, 3 );
    function wc_cp_component_options_per_page( $results_count, $component_id, $composite ) {
	return 50;
}

add_filter( 'woocommerce_composite_component_summary_max_columns', 'wc_cp_summary_max_columns', 10, 2 );
    function wc_cp_summary_max_columns( $cols, $composite ) {
	return 6;
}

add_filter( 'woocommerce_composite_component_lazy_load', '__return_false', 10 );




add_filter('woocommerce_composite_component_option_data', function($data){
    $product_id = $data['option_id'];

    $rc_product = wc_get_product( $product_id );
    $rc_product_shord_description = $rc_product->get_short_description();

    $data['option_color'] = get_field('ice_color', $product_id);
    $data['option_description'] = $rc_product_shord_description;
    return $data;
}, 10, 1);