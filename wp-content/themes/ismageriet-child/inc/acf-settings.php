<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Ismageriet opsætning',
		'menu_title'	=> 'Ismageriet opsætning',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Lukkedage',
		'menu_title'	=> 'Lukkedage',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}
