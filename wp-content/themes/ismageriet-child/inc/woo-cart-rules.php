<?php

//Delete Cart Item Permalink
add_filter( 'woocommerce_cart_item_permalink', '__return_null' );


// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ){

    //If the product in the cart has one of these IDs the script will show address fields 
    $products_needs_address = array(194241,192373,192372,1139);


    $hide_address_fields = true;
    foreach (WC()->cart->get_cart() as $cart_item){
        if(in_array($cart_item['product_id'], $products_needs_address)){
            $hide_address_fields = false;
        }
    }
    
    //Unset address fields if they not needed
    if($hide_address_fields){
    //    unset($fields['billing']['billing_address_1']);
    //    unset($fields['billing']['billing_city']);
    //    unset($fields['billing']['billing_postcode']);
    //    unset($fields['shipping']['shipping_address_1']);
    //    unset($fields['shipping']['shipping_city']);
    //    unset($fields['shipping']['shipping_postcode']);
    }

    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);

    unset($fields['order']['order_comments']);

    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);


    return $fields;
}


add_action( 'woocommerce_before_checkout_billing_form', 'add_new_billing_form_title' );
function add_new_billing_form_title() {
    echo '<h3 class="custom-billing-title">Faktureringsadresse</h3>';
}


// ---------------------------------
// 1) Make original email field half width
// 2) Add new confirm email field
add_filter( 'woocommerce_checkout_fields' , 'add_email_verification_field_checkout' );

function add_email_verification_field_checkout( $fields ) {
	$fields['billing']['billing_email']['class'] = array( 'form-row-first' );
	$fields['billing']['billing_em_ver'] = array(
	    'label' => 'Bekræft e-mailadresse',
	    'required' => true,
	    'class' => array( 'form-row-last' ),
	    'clear' => true,
	    'priority' => 81,
	);
	return $fields;
}

// ---------------------------------
// 3) Generate error message if field values are different
add_action('woocommerce_checkout_process', 'check_matching_email_addresses');
function check_matching_email_addresses() {
    $email1 = $_POST['billing_email'];
    $email2 = $_POST['billing_em_ver'];
    if ( $email2 !== $email1 ) {
        wc_add_notice( '<strong>E-mailadresse</strong> matcher ikke', 'error' );
    }
}


