<?php

add_filter( 'woocommerce_bundled_item_is_optional_checked', 'wc_pb_is_optional_item_checked', 10, 2 );
function wc_pb_is_optional_item_checked( $checked, $bundled_item ) {

	if ( ! isset( $_GET[ 'update-bundle' ] ) ) {
		$checked = true;
	}

	return $checked;
}


add_filter( 'woocommerce_continue_shopping_redirect', 'bbloomer_change_continue_shopping' );
function bbloomer_change_continue_shopping() {
   return wc_get_page_permalink( 'shop' );
}


// Add back to store button on WooCommerce cart page
add_action('woocommerce_after_cart_table', 'themeprefix_back_to_store');
// add_action( 'woocommerce_cart_actions', 'themeprefix_back_to_store' );

function themeprefix_back_to_store() { ?>
<a href="https://booking.ismageriet.dk/" tabindex="1" class="button wc-forward groen-btn">Fortsæt med at handle</a>

<?php
}


// =============================================
// Cange add to cart button text
function kathart_custom_change_single_add_to_cart_text( $text, $product ) {

	// Set a button text for a specific variable
	if ( $product->is_type( 'variable' ) ) {
		$text = __( 'Videre til isvalg' );
	}

	// Ændre teksten på kategoriniveau
//	$category = get_term_by( 'isbokse-11', 'isbokse-22', 'isbokse-33' );
//	if ( $category && in_array( $category->term_id, $product->get_category_ids() ) ) {
//		$text = __( 'Ny tekst' );
//	}

	return $text;
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'kathart_custom_change_single_add_to_cart_text', 10, 2 );