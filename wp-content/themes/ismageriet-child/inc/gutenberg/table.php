<?php
add_action('init', function () {
    wp_register_style('awp-block-styles', get_template_directory_uri() . '/style.css', false);
    
    register_block_style('core/table', [
        'name' => 'open-hours',
        'label' => __('Open hours', 'txtdomain'),
        'style_handle' => 'awp-block-styles',

    ]);

});
