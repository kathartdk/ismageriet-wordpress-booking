<?php
add_action('init', function () {
    wp_register_style('awp-block-styles', get_template_directory_uri() . '/style.css', false);

    register_block_style('core/heading', [
        'name' => 'box-heading',
        'label' => __('Sonsie One', 'txtdomain'),
        'style_handle' => 'awp-block-styles',

    ]);

	register_block_style('core/heading', [
        'name' => 'title-lines',
        'label' => __('Side titel', 'txtdomain'),
        'style_handle' => 'awp-block-styles',

    ]);

});
