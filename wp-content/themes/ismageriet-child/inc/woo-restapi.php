<?php

define( 'INTEGREAT_STAGING_ENDPOINT', '' );
define( 'INTEGREAT_LIVE_ENDPOINT', 'https://engine.netsupport.dk:7120/orders/v2/' );
define( 'INTEGREAT_LIVE_ENDPOINT_BOOKING', 'https://engine.netsupport.dk:7120/orders/v3/' );
define( 'INTEGREAT_SECRET', 'YZrxnypjaN8WOllWZBCfMLiJA8b6C1Tb1o' );
define( 'INTEGREAT_SECRET_BOOKING', 'm5Y8AGgeXTRugUxYke6Kpsae42AjgJv4PN' );
define( 'SYNC_CUTOFF_DATE', '2020-01-01 23:59:59');

/*
 * Add to the Order API response.
*/
function wv_modify_wc_shop_order_response( $response, $object, $request ) {
	// check paramters
	if($request->get_param('fields') == 'ids') {
		// set response to only id
		return [ 'id' => $response->data['id'] ];
	}

	return $response;
}

add_filter( 'woocommerce_rest_prepare_shop_order_object', 'wv_modify_wc_shop_order_response', 20, 3 );

/*
 * Setup a CRON job to run every minute to call InteGreat
 */
function yanco_run_every_minute_cron() {
    // Call InteGreat with "keep alive" signal
    $result = yanco_call_integreat( 'none' );
}

add_action( 'yanco_run_every_minute_cron_event', 'yanco_run_every_minute_cron' );
if (! wp_next_scheduled ( 'yanco_run_every_minute_cron_event' )) {
    //wp_schedule_event( time(), 'every_minute', 'yanco_run_every_minute_cron_event' );
}

/*
 * Setup a CRON job to run every 5 minutes to call InteGreat
 */
function yanco_run_every_five_minutes_cron() {
    // Call InteGreat with unsynced orders
    //yanco_get_all_unsynced_orders();
}

add_action( 'yanco_run_every_five_minutes_cron_event', 'yanco_run_every_five_minutes_cron' );
if (! wp_next_scheduled ( 'yanco_run_every_five_minutes_cron_event' )) {
    //wp_schedule_event( time(), 'every_five_minutes', 'yanco_run_every_five_minutes_cron_event' );
}

/*
 * When a shop_order post is saved, set the order_synced_status to FALSE
 * IF It is not already set to TRUE
 */
//add_action( 'woocommerce_thankyou', 'sync_with_integreat', 10, 1 );
//add_action( 'woocommerce_checkout_update_order_meta', 'sync_with_integreat', 10, 1 );
add_action( 'woocommerce_order_status_processing', 'sync_with_integreat', 10, 1);
//add_action( 'woocommerce_payment_complete', 'sync_with_integreat', 10, 1);
function sync_with_integreat( $order_id ) {
    update_post_meta( $order_id, 'order_synced_status', 0 );
    $synced_to_integrate = yanco_call_integreat( $order_id );
	//wp_mail( 'bivol.leonid@gmail.com', 'Order Status' . $order_id . get_site_url(), 'Update: ' . $synced_to_integrate . ' state ' .  $order_id );
    if( $synced_to_integrate === true ) {
        update_post_meta( $order_id, 'order_synced_status', 1 );
    }
}

// add_shortcode('integreat', 'yanco_call_integreat');
function yanco_call_integreat( $order_id = 'none' ) {
	$integreat_endpoint_url= '';

	if( get_site_url() == 'http://woocommerce-442350-1439348.cloudwaysapps.com' ) {
		$integreat_endpoint_url = INTEGREAT_STAGING_ENDPOINT . INTEGREAT_SECRET . '/' . $order_id;
	} elseif ( strpos(get_site_url(), 'booking.ismageriet.dk') ) {
		$integreat_endpoint_url = INTEGREAT_LIVE_ENDPOINT_BOOKING . INTEGREAT_SECRET_BOOKING . '/' . $order_id;
	} elseif ( strpos(get_site_url(), 'ismageriet.dk') ) {
		$integreat_endpoint_url = INTEGREAT_LIVE_ENDPOINT . INTEGREAT_SECRET . '/' . $order_id;
	}

	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_URL, $integreat_endpoint_url );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec( $ch );
	$status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
	curl_close( $ch );

    if( $status == 200 ) {

        if( $order_id != 'none' ) {
            // If the $order_id isn't 'none" it's because it was an order sync to InteGreat
            // Update the post meta as InteGreat repsonded with "OK"
            // {"result":"OK","message":"Order received ok","errorCode":"none"}
            update_post_meta( $order_id, 'order_synced_status', 1 );
        }

        // {"result":"OK","message":"Alive msg received ok","errorCode":"none"}
        // This was a "keep alive" signal, also responsd "OK" from InteGreat
        return true;
    }

    return false;
}

/*
 * One time action, set all shop_order posts, order_synced_status to FALSE
 */
// add_action('wp_footer', 'update_all_order_synced_status');
function update_all_order_synced_status() {
    $query = new WC_Order_Query( array(
        'limit' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'return' => 'ids',
    ) );
    $orders = $query->get_orders();

    foreach( $orders as $key => $order_id ) {
        update_post_meta( $order_id, 'order_synced_status', 0 );
    }
}

function yanco_get_all_unsynced_orders() {
    global $wpdb;
    $include_post_ids = array();

    $all_unsyned_orders_cut_off_date_sql = "SELECT post_id FROM {$wpdb->prefix}postmeta
    WHERE meta_key = '_completed_date' 
    AND meta_value >= '". SYNC_CUTOFF_DATE ."'";

    $unsyned_orders_after_cutoff_result = $wpdb->get_results( $all_unsyned_orders_cut_off_date_sql );
    foreach ( $unsyned_orders_after_cutoff_result as $order )  {
        $include_post_ids[] = $order->post_id;
    }

    // Only proceed if there are unsynced orders
    if( !empty( $include_post_ids ) ) {
        $all_unsyned_orders_to_sync_sql = "SELECT post_id FROM {$wpdb->prefix}postmeta
        WHERE meta_key = 'order_synced_status' 
        AND meta_value = 0 
        AND post_id IN(" . implode( ',', $include_post_ids ) . ") ";

        $all_unsyned_orders_to_sync_sql_result = $wpdb->get_results( $all_unsyned_orders_to_sync_sql );

        if( !empty( $all_unsyned_orders_to_sync_sql_result ) )
        foreach ( $all_unsyned_orders_to_sync_sql_result as $order_to_sync )  {
            // TODO: Call Integreat
            yanco_call_integreat( $order_to_sync->post_id );
        }
    }
}
