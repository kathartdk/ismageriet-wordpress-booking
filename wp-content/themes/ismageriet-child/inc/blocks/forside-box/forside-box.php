<?php
/**
 * This is the template that renders the team block.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'forside-boks-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'forside-boks-block';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="block <?php echo esc_attr($className); ?> <?php the_field('box_size'); ?>">

    <a href="<?php the_field('page_link'); ?>" >
    <?php
    $baggrundsbillede = get_field( 'backgroundsbillede' );
    if ( $baggrundsbillede ) : ?>
        <div class="image-wrapper">
            <img class="img-responsive" src="<?php echo esc_url( $baggrundsbillede['url'] ); ?>" alt="<?php echo esc_attr( $baggrundsbillede['alt'] ); ?>" />
        </div>
    <?php endif; ?>

    <?php if ( $header_text = get_field( 'header_text' ) ) : ?>
        <h2 class="<?php the_field('title_color'); ?>"><?php echo esc_html( $header_text ); ?></h2>
    <?php endif; ?>

    <div class="border"></div>

    <?php if ( $knap_tekst = get_field( 'knap_tekst' ) ) : ?>
        <span class="action-btn shift-left align-bottom  <?php the_field('knap_frave'); ?>">
            <span><?php echo esc_html( $knap_tekst ); ?></span>
        </span>
    <?php endif; ?>
    </a>
</div>

