<?php
/**
 * This is the template that renders the team block.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'ugens-is-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'ugens-is-block';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="block <?php echo esc_attr($className); ?> ice-of-the-week">

	<a href="<?php the_field('page_link'); ?>" >
	<?php
	$baggrundsbillede = get_field( 'backgroundsbillede' );
	if ( $baggrundsbillede ) : ?>
		<img class="img-responsive" src="<?php echo esc_url( $baggrundsbillede['url'] ); ?>" alt="<?php echo esc_attr( $baggrundsbillede['alt'] ); ?>" />
	<?php endif; ?>

	<?php if ( $ugens_is = get_field( 'ugens_is' ) ) : ?>
		<header class="ice-header">
			<h2><?php echo esc_html( $ugens_is ); ?></h2>
		</header>
	<?php endif; ?>

	<?php if ( $is_navn = get_field( 'is_navn' ) ) : ?>
		<p class="ice-name"><?php echo esc_html( $is_navn ); ?></p>
	<?php endif; ?>
	</a>
</div>