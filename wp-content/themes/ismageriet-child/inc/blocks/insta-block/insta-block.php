<?php
/**
 * This is the template that renders the team block.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'insta-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'insta-block';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="block <?php echo esc_attr($className); ?> gallery-insta instagram">
	
	<a href="<?php the_field('page_link'); ?>" >
		
		<?php
		$baggrundsbillede = get_field( 'backgroundsbillede' );
		if ( $baggrundsbillede ) : ?>
			<div class="image-wrapper">
				<img class="img-responsive" src="<?php echo esc_url( $baggrundsbillede['url'] ); ?>" alt="<?php echo esc_attr( $baggrundsbillede['alt'] ); ?>" />
			</div>
		<?php endif; ?>

		<div class="instagram-banner">
			<i class="instagram-icon fa fa-instagram"></i>
			<span class="instagram-hashtag">
				#ismageriet
			</span>
		</div>

    </a>
</div>
