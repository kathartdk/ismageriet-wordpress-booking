jQuery(document).ready(function(){

	jQuery(document).on('click', '.component_option_radio_button .tool', function(){
		var $item = jQuery(this).next().addClass('current');

		jQuery('.thumbnal-short-description.active').not('.current').removeClass('active');

		$item.toggleClass('active').removeClass('current');
	});


	jQuery(document).on('click', function(e){
		if(!jQuery(e.target).hasClass('tool') && !jQuery(e.target).closest('.thumbnal-short-description').length){
			jQuery('.thumbnal-short-description.active').removeClass('active');
		}
	});

});
